# Prikaz naucnog rada

Repozitorijum: ```git clone https://danijela_simic@bitbucket.org/geoalg1819/journal.git```

Materijal moze biti u vidu:

   * članka (3-4 strane, LaTeX) ILI
   * slajdova (10-15, LaTeX - beamer)

Po završetku rada, izvorni materijal plus PDF poslati mejlom profesoru, sa asistentkinjom u cc.

Radovi ce biti ocenjeni nakon odbrane.

Jedan od ciljeva ovog projekta da u izabranom radu uočite šta je najbitnite i da to prikažete i istaknete.

U okviru materijala treba obraditi sledeća pitanja:

   * Ko su (i odakle su) autori rada
   * Gde i kada je objavljen rad
   * Najpre kratak, neformalan opis problema i zašto je bitan
   * Opis glavnih rezultata (teoreme/dokazi, algoritmi, implementacije, ..)
   * Opis primena
   * Kako se opisani rezultati odnose na druge relevantne rezultate (nijedan rad nije nezavisan od svega ostalog; dakle - da li donosi uopštenje tvrdjenja, unapredjeni algoritam, nove primene, itd)

Držati se preporučenog obima materijala, prekoračenja u bilo kom smeru smatraju se propustom. 

