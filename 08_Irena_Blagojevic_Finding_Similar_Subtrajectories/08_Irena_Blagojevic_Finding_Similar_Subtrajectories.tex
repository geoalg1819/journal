\documentclass{beamer}

\usepackage[british]{babel}
\usepackage{graphicx,hyperref,ru,url}
\usepackage[utf8]{inputenc}

\bibliographystyle{plain}

\title[]{Nalaženje dugih i sličnih delova trajektorija}

\subtitle{\small Kevin Buchin, Maike Buchin, Marc van Kreveld, Jun Luo}

\author[]{Irena Blagojević\\{\tiny \vspace{3mm} Matematički fakultet\\\vspace{-2mm} Univerzitet u Beogradu}}

\institute[]{Computational Geometry 44 (2011)\\ {\tiny \url{https://www.sciencedirect.com/science/article/pii/S0925772111000344}}}

\date[]{31. decembar 2018.}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\section{Uvod}
\begin{frame}
  \frametitle{Uvod}
  \begin{itemize}
  \item Analiza putanja kretanja ({\bf{trajektorija}}) ljudi, životinja, vozila, prirodnih pojava, itd.
  \item Velike količine podataka o putanjama
  \item Ključna operacija -- određivanje sličnosti trajektorija
  \item Potrebna mera uzima u obzir vremenske aspekte trajektorije
  \end{itemize}

  \begin{columns}
    \column{0.45\linewidth}
    \begin{figure}
      \includegraphics[scale=0.2]{diff_speed}
    \end{figure}
    \column{0.45\linewidth}
    \begin{figure}
      \includegraphics[scale=0.15]{clustering}
    \end{figure}
  \end{columns}
\end{frame}

\section{Autori}
\begin{frame}
  \frametitle{Autori}
  \begin{itemize}
  \item Kevin Buchin {\footnotesize (Tehnološki univerzitet u Ajndhovenu, Holandija)}\\
    {\footnotesize Profesor u okviru grupe za algoritmiku}
  \item Maike Buchin {\footnotesize (Tehnološki univerzitet u Ajndhovenu, Holandija)}\\
    {\footnotesize Profesor na Univerzitetu u Bohumu, Nemačka -- katedra za matematiku i računarstvo}
  \item Marc van Kreveld {\footnotesize (Univerzitet u Utrehtu, Holandija)}\\
    {\footnotesize Profesor geometrijskih algoritama i primena, jedan od autora knjige ,,Computational Geometry: algorithms and applications''}
  \item Jun Luo {\footnotesize (Instituti za napredne tehnologije u Šenženu, Kineska akademija nauka)}\\
    {\footnotesize Bivši profesor na institutu, danas se bavi veštačkom inteligencijom}
  \end{itemize}
\end{frame}

\section{O radu}
\begin{frame}
  \frametitle{O radu}
  \begin{itemize}
  \item Časopis ,,Computational Geometry'', 2011. godine
  \item Algoritam za nalaženje najsličnijih podtrajektorija dve trajektorije
    \begin{itemize}
    \item Sa i bez vremenskog pomeraja
    \item Unapređenje postojećih algoritama
    \item Prosečno Euklidsko rastojanje između podtrajektorija u odgovarajućim trenucima vremena
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Opis problema}
\begin{frame}
  \frametitle{Opis problema}
  \begin{block}
    {Prosečno rastojanje}
    Rastojanje između dela $\tau_1$ počevši od trenutka $t_s$ i dela $\tau_2$ počevši od trenutka $t_s+t_{shift}$, oba u trajanju od $T>0$:
    $$\dfrac{\int_{t_s}^{t_s+T} d(\tau_1(t),~\tau_2(t+t_{shift}))dt}{T}$$
    {\footnotesize $\tau_1$, $\tau_2$ -- parametrizacije dve trajektorije kroz vreme\\}
    {\footnotesize $d(\cdot,\cdot)$ -- Euklidsko rastojanje između $\tau_1(\ldotp)$ i $\tau_2(\ldotp)$}
  \end{block}

  {\bf{\small Traži se kombinacija $T$, $t_s$ i $t_{shift}$ koja daje najmanju različitost}}
\end{frame}

\section{Postojeći algoritmi}
\begin{frame}
  \frametitle{Postojeći algoritmi}
  \begin{itemize}
  \item Postoji algoritam vremenske složenosti $\mathcal{O}(n)$ koji rešava problem sa fiksnim trajanjem i bez vremenskog pomeraja
    \vspace{3mm}
  \item Za pristup rešavanja problema sa nefiksnim trajanjem i bez vremenskog pomeraja postoji algoritam složenosti $\mathcal{O}(n^2)$
    \vspace{3mm}
  \item Algoritmi sa vremenskim pomerajem koriste aproksimacije umesto tačnog rešenja bez garancije kvaliteta u $\mathcal{O}(n^4)$
  \end{itemize}
\end{frame}

\section{Glavni rezultati}
\begin{frame}
  \frametitle{Glavni rezultati}
  \begin{block}{$(1+\varepsilon)$-aproksimacija}
    Za fiksiranu i nefiksiranu dužinu trajanja.\\
    Ako optimalno rešenje ima rastojanje $D$, onda predloženi algoritam pronalazi rešenje sa rastojanjem ne većim od $(1+\varepsilon) \cdot D$.
  \end{block}

  \begin{itemize}
  \item U opštem slučaju, vremenska složenost je $\mathcal{O}(n^4/\varepsilon)$ i $\mathcal{O}(n^4/\varepsilon^2)$
  \item Ako su tačke posmatranja $t_1$, $t_2$, ... ravnomerno raspoređene po trajektoriji, onda je vremenska složenost poboljšana za linearni faktor --- $\mathcal{O}(n^3/\varepsilon)$ i $\mathcal{O}(n^3/\varepsilon^2)$
  \end{itemize}
\end{frame}

\subsection{Algoritam za podtrajektorije bez vremenskog pomeraja}
\begin{frame}
  \frametitle{\hspace{-12mm}Algoritam za podtrajektorije bez vremenskog pomeraja}
  \begin{columns}
    \column{0.40\textwidth}
    \begin{figure}
      \includegraphics[scale=0.16]{geometrijska_interpretacija}
    \end{figure}
    \column{0.72\textwidth}
    \begin{itemize}
    \item Dužina trajanja fiksna
      \begin{itemize}
      \item {\bf{Linearan}} prolaz ,,vremenskog okvira'' kroz funkciju rastojanja
      \end{itemize}
      \vspace{2mm}
    \item Dužina trajanja nije fiksna
      \begin{itemize}
      \item Brišuća prava sleva nadesno
      \item Minimalni nagib prave kroz $(t_{end}, F(t_{end}))$ i neku tačku u trenutku $t$ pre $t_{end}$
      \item Tačke događaja: $break$, $merge$, $create$ i $discard$ -- obrada u $\mathcal{O}(1)$
      \item Najsličnije podtrajektorije u ${\bf{\mathcal{O}(n)}}$
      \end{itemize}
    \end{itemize}
  \end{columns}
\end{frame}

\subsection{Aproksimacioni algoritmi za podtrajektorije sa vremenskim pomerajem}
\begin{frame}
  \frametitle{\hspace{-12mm}Algoritmi za podtrajektorije sa vremenskim pomerajem}
  \begin{itemize}
  \item Koristi se funkcija rastojanja konveksnih poligona
  \item {\bf{Polinomijlni $(1+\varepsilon)$-aproksimacioni}} algoritam za slučaj fiksirane dužine trajanja -- $\mathcal{O}(n^4/\varepsilon)$
    \begin{itemize}
    \item Računa se {\bf{placement space}}
    \item Za svaku ćeliju se minimizuje površina ispod intervala dužine $\overline{T}$
    \end{itemize}
  \item Redukcija problema nefiksirane dužine trajanja na više instanci fiksirane dužine -- $\mathcal{O}(n^4/\varepsilon^2)$
  \end{itemize}
  \vspace{-1em}
  \begin{figure}
    \includegraphics[scale=0.25]{placement_space}
  \end{figure}
\end{frame}

\section{Primene}
\begin{frame}
  \frametitle{Primene}
  \begin{itemize}
  \item Predviđanje putanja kretanja (npr. uragana)
  \item Klasterovanje -- vremensko-prostorno istraživanje podataka
  \item Analiza obrazaca kretanja (jata, konvoji, migracije, itd.)
  \end{itemize}
\end{frame}

\section{Slični radovi}
\begin{frame}
  \frametitle{Slični radovi}
  \begin{itemize}
  \item Algoritmi samo za diskretne trajektorije \cite{SinhaMark}
  \item Algoritmi za kontinualni slučaj, ali pod pretpostavkom da trajektorije počinju u istom trenutku u vremenu, bez razmatranja podtrajektorija \cite{NanniPedreschi}
  \item Algoritam za kontinualni slučaj koji koristi sličnost podtrajektorija i vremenske pomeraje \cite{vanKreveldJun}
    \begin{itemize}
    \item Aproksimacija integrala umesto tačnog izračunavanja
    \end{itemize}
  \item Klasterovanje podtrajektorija: modeli i algoritmi \cite{cluster}
  \end{itemize}
\end{frame}

\section{Zaključak}
\begin{frame}
  \frametitle{Zaključak}
  \begin{itemize}
  \item Ista početna tačka --- konstruisan linearan algoritam
  \item Ne nužno ista --- $(1+\varepsilon)$-aproksimacioni algoritam
    \begin{itemize}
    \item {\bf{Otvoreno pitanje da li postoji efikasnije rešenje}}
    \item Bolje ponašanje je moguće u praksi, u slučaju kada su tražene podtrajektorije kratke u odnosu na ukupnu dužinu trajektorije
    \item Pojednostavljivanje trajektorija prilikom preprocesiranja
    \end{itemize}
  \item U budućnosti:
    \begin{itemize}
    \item Da li je ova mera pogodna za klasterovanje
    \item Efikasnija analiza više trajektorija
    \end{itemize}
  \end{itemize}
\end{frame}

\section*{}
\begin{frame}
  \begin{center}
    \Large{Hvala na pažnji!}
  \end{center}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Literatura}
  \appendix
  \bibliography{seminarski}
\end{frame}
\end{document}
